# Tanuki Racing Application

Main project for all GitLab hands on events, main project here: https://gitlab.com/gitlab-learn-labs/sample-projects/tanuki-racing/-/edit/main/README.md

## Contribution Guidlines

Tanuki Racing is actively maintained by the Demo Architecture and GitLab team. To get started, please click **Request Access** at the top of the page to start contributing. Feel free to contribute back to the project by opening a merge request to fix a broken pipeline, piece of code, or add an additional course. Please remeber to assign the MR to @lfstucker and add an entry to the RELEASE.md to keep track of any changes or updates. **Please note you must be a GitLab employee to contribute to the project**
